import scrapy


class QuotesSpider(scrapy.Spider):
    name = 'quotes_spider'

    def start_requests(self):
        url = 'http://quotes.toscrape.com/'
        yield scrapy.Request(url=url, callback=self.my_custom_parse, cb_kwargs={'page': 1})

    def my_custom_parse(self, response, page):
        quotes = response.css(".text")
        for quote in quotes:
            yield {
                "page": page
            }

        page = page + 1
        next_url = 'http://quotes.toscrape.com/page/{page_number}'.format(page_number=page)

        yield scrapy.Request(url=next_url, callback=self.my_custom_parse, cb_kwargs={'page': page})

    # def parse(self, response, **kwargs):
    #     pass
