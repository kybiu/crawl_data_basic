from selenium import webdriver
import time


def login(driver, url):
    driver.get(url)
    time.sleep(1)

    user_name = driver.find_element_by_xpath("//input[@type='text']")
    user_name.send_keys("dashboard")

    password = driver.find_element_by_xpath("//input[@type='password']")
    password.send_keys("dashboard")

    submit_button = driver.find_element_by_xpath("//button[@type='submit']")
    submit_button.click()
    time.sleep(30)


def main():
    driver = webdriver.Chrome()
    url = 'https://test.salebot.ftech.ai/dashboard/ui/salebot'

    # Login
    login(driver, url)


if __name__ == '__main__':
    main()
