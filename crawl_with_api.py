import cloudscraper
import json


def login(scraper):
    url = 'https://test.salebot.ftech.ai/dashboard/backend/api/v1/Identity/login'

    login_info = {
        'client_id': "eclazz",
        'grant_type': "password",
        'password': "dashboard",
        'remember': False,
        'username': "dashboard"
    }

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': "Chrome"
    }

    response = scraper.post(url=url,
                            json=login_info,
                            headers=headers)
    try:
        response_text = json.loads(response.text)
        token = response_text["token"]
        cookies = response.cookies
        return token, cookies
    except Exception as e:
        print("Error when login: " + str(e))


def crawl_dashboard_info(scraper, from_date, to_date, shop_id):
    url = 'https://test.salebot.ftech.ai/dashboard/backend/api/v1/dashboard/getDashboardInfo?fromDate={from_date}&toDate={to_date}&shopId={shop_id}'
    url = url.format(from_date=from_date, to_date=to_date, shop_id=shop_id)
    scraper = cloudscraper.create_scraper()

    response = scraper.get(url)

    try:
        response_text = json.loads(response.text)
        a = 0
    except Exception as e:
        print(e)


def main():
    scraper = cloudscraper.create_scraper()
    token, cookies = login(scraper)
    crawl_dashboard_info(scraper, '20210113', '20210327', '1059845080701224')


if __name__ == '__main__':
    main()
